from pydantic import BaseModel, Field
from typing import Optional, ClassVar, List, Dict


class Movie(BaseModel):
    id: Optional[int] = None #Indicamos que es opcional
    title: str = Field(default="Titulo Pelicula", min_length=5, max_length=15)
    overview: str = Field(default="Esta es la descripcion de la pelicula", min_length=15, max_length=50)
    year: int = Field(le=2024)
    rating: float = Field(ge=1, le=10)
    category: str = Field(min_length=5, max_length=15)

    movies: ClassVar[List[Dict]] = [
    {
        "id": 1,
        "title": "Avatar",
        "overview": "En un exhuberante planeta llamado pandora viven unos monos azules",
        "year":"2009",
        "rating":7.8,
        "category":"Accion"
    },
    {
        "id": 2,
        "title": "The Hunger Games on Fire",
        "overview": "Best Movie Ever",
        "year":"2014",
        "rating":10.0,
        "category": "Comedia"
    }
]